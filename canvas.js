let range = document.getElementById("range");
let reset = document.getElementById("reset");
let brush = document.getElementById("brush");
let eraser = document.getElementById("eraser");
let circle = document.getElementById("circle");
let triangle = document.getElementById("triangle");
let rect = document.getElementById("rect");
let text = document.getElementById("text");
let fontsize = document.getElementById("fontsize");
let fonttype = document.getElementById("fonttype");
let redo = document.getElementById("redo");
let undo = document.getElementById("undo");
var canvas = document.getElementById('mycanvas')
var ctx = canvas.getContext('2d')

var array=new Array();
var steps=-1;

let x1= 0
let y1= 0

let x2= 0
let y2= 0

const hasTouchEvent = 'ontouchstart' in window ? true : false
const downEvent = hasTouchEvent ? 'ontouchstart' : 'mousedown'
const moveEvent = hasTouchEvent ? 'ontouchmove' : 'mousemove'
const upEvent = hasTouchEvent ? 'touchend' : 'mouseup'

let isMouseActive = false
let historyDeta = [];
let index = 0;
let max_index = 0;

initial();

function initial(){
    historyDeta[0] = ctx.getImageData(0, 0, canvas.width, canvas.height);
}

canvas.addEventListener(downEvent, function(e){
  isMouseActive = true
  x1 = e.offsetX
  y1 = e.offsetY
  color();
  if (type == 3) {
    ctx.fillText(text.value,x1,y1);
  }
  ctx.lineCap = 'round'
  ctx.lineJoin = 'round'
})

canvas.addEventListener(moveEvent, function(e){
      if(!isMouseActive){
        return
      }
      if (!clear){
        if (type == 1){
            x2 = e.offsetX
            y2 = e.offsetY
            ctx.beginPath()
            ctx.moveTo(x1, y1)
            ctx.lineTo(x2, y2)
            ctx.stroke()
            x1 = x2
            y1 = y2
        }
        else if (type == 2) {
            ctx.putImageData(historyDeta[index], 0, 0);
            x2 = e.offsetX;
            y2 = e.offsetY;
            ctx.beginPath();
            ctx.fillRect(x1,y1,x2-x1,y2-y1);
            ctx.stroke();
        }
        else if (type == 4) {
            ctx.putImageData(historyDeta[index], 0, 0);
            x2 = e.offsetX;
            y2 = e.offsetY;
            ctx.beginPath();
            let radius = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
            if (radius >= 758 - x1) radius = 758 - x1;
            if (radius >= x1) radius = x1;
            if (radius >= 456 - y1) radius = 456 - y1;
            if (radius >= y1) radius = y1;
            ctx.arc(x1, y1, radius, 0 * Math.PI, 2 * Math.PI);     
            ctx.fill();
            ctx.stroke();
        }
        else if (type == 6) {
            ctx.putImageData(historyDeta[index], 0, 0);
            x2 = e.offsetX;
            y2 = e.offsetY;
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            if (x1 >= 379) {
                if (x2 <= 2*x1 - 758){
                    x2 = 2*x1 - 758;
                }
            }
            else {
                if (x2 >= 2*x1)
                    x2 = 2*x1;                
            }
            ctx.lineTo(x2, y2);
            let dot_x = 2 *x1 -x2;
            if (dot_x < 0) dot_x = 0;
            if (dot_x >= 758) dot_x = 758;
            ctx.lineTo(dot_x, y2);
            ctx.lineTo(x1, y1);              
            ctx.fill();
            ctx.stroke();
        }
      }
      else {
        ctx.save();
        ctx.globalCompositeOperation = "destination-out";
        x2 = e.offsetX
        y2 = e.offsetY
        ctx.beginPath()
        ctx.moveTo(x1, y1)
        ctx.lineTo(x2, y2)
        ctx.stroke()
        x1 = x2
        y1 = y2
        ctx.clip();
        ctx.clearRect(0,0,canvas.width,canvas.height);
        ctx.restore();
      }
})

function color(){
    colorWell = document.getElementById("color");
    ctx.strokeStyle = colorWell.value;
    ctx.fillStyle = colorWell.value;
}

canvas.addEventListener(upEvent, function(e){
  isMouseActive = false;
  let data;
  data = ctx.getImageData(0, 0, canvas.width, canvas.height);
  saveData(data);
  console.log(historyDeta.length)
})

range.onchange = function(){
    ctx.lineWidth = this.value;
};

eraser.onclick = function () {
    clear = true;
    type = 5;
    document.body.style.cursor = "url('img/cursor_eraser.png'),auto";
};

brush.onclick = function () {
    clear = false;
    type = 1;
    document.body.style.cursor = "url('img/cursor_brush.png'),auto";
};
text.onclick = function() {
    clear = false;
    type = 3;
    document.body.style.cursor = "url('img/cursor_text.png'),auto";
};
circle.onclick = function () {
    clear = false;
    type = 4;
    document.body.style.cursor = "url('img/cursor_circle.png'),auto";
};

rect.onclick = function () {
    clear = false;
    type = 2;
    document.body.style.cursor = "url('img/cursor_rect.png'),auto";
};

triangle.onclick = function () {
    clear = false;
    type = 6;
    document.body.style.cursor = "url('img/cursor_triangle.png'),auto";
};

function saveData (data) {
    (historyDeta.length === 50) && (historyDeta.shift());
    index = index + 1;
    historyDeta[index] = data;
    max_index = index;
}

undo.onclick = function(){
    if(index <= 1){
        ctx.putImageData(historyDeta[0], 0, 0);
        if (index == 1) index = index - 1;
    }
    else {
        ctx.putImageData(historyDeta[index-1], 0, 0);
        index = index - 1;
    }
};

redo.onclick = function(){
    if(index < max_index){
        ctx.putImageData(historyDeta[index+1], 0, 0);
        index = index + 1;
    }
};

reset.onclick = function(){
    location.reload(); 
};

fontsize.onchange = function(){
    ctx.font =  fontsize.value+"px "+fonttype.value;
};

fonttype.onchange = function(){
    ctx.font =  fontsize.value+"px "+fonttype.value;
};

save.onclick = function () {
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    saveA.download = "zspic" + (new Date).getTime();
    saveA.target = "_blank";
    saveA.click();
};

function upload(e){
    let file = new FileReader();
    file.readAsDataURL(e.target.files[0]);
    file.onload = (e) =>{
      var img = new Image();
      img.src = e.target.result;
      img.onload = () =>{
        ctx.drawImage(img, 0, 0);
         storeTemp();
      }
    }
  }
