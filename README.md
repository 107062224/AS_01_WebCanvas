# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use this website

    1.there are many buttons in my website, you can use them by click.
    
    2.if you want to type something, you only need to click textbox
    and type somthing inside it.
    
    3.When you upload something, I design place it in upper left 
    corner.
    
    4.you can adjust brush size and eraser size by the same way.
    
    5.you can also adjust brush color and shapes color by the same way.
    



### Gitlab page link

    https://107062224.gitlab.io/AS_01_WebCanvas

    

<style>
table th{
    width: 100%;
}
</style>